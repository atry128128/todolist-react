class TodoList extends React.Component {

    state = {
        tasks: [],
        valueInput: "",
        idCounter: 0
    }

    handleAddClick() {
      
        if (this.state.valueInput !== "") {

            let newTask = this.state.tasks.concat({ id: [this.state.idCounter++], title: [this.state.valueInput], isDone: "false" })
            this.setState({
                tasks: newTask,
                valueInput: ""
            })
        }
    }

    handleCheckboxChange = (id) => {

        let tasksToChange = this.state.tasks.map(task => {
            if (task.id === id) {
                task.isDone = !task.isDone
            } return task
        })
        this.setState({
            tasks: tasksToChange
        })
    }


    handleChange = (e) => {
        this.setState({
            valueInput: e.target.value
        })
    }

    handleRemoveClick = id => {
        const newTasks = this.state.tasks.filter(task => task.id !== id)

        setTimeout(() => {
            this.setState({
                tasks: newTasks
            })
        }, 600);

    }

    render() {
        return (
            <React.Fragment>
                <h1 id="topic-font">ToDo List</h1>
                <input className="input" type="text" placeholder="type a task.." value={this.state.valueInput} onChange={this.handleChange} />
                <button className="button-add" onClick={this.handleAddClick.bind(this)}><i className="fas fa-plus"></i></button>
                <ListOfTasks tasks={this.state.tasks} handleClick={this.handleRemoveClick} handleCheckboxChange={this.handleCheckboxChange} />
            </React.Fragment>
        )
    }
}

const ListOfTasks = (props) => {

    let tasks = props.tasks.map(task => <Task handleCheckboxChange={props.handleCheckboxChange} handleClick={props.handleClick} key={task.id} task={task} />)
    return (
        <ul>{tasks}</ul>
    )
}

const Task = (props) => {
    return (
        <li className={props.task.isDone ? "" : "isntDone"} key={props.key} id={props.task.id}>
            <button className="checkbox" value={true} onClick={() => props.handleCheckboxChange(props.task.id)} >{props.task.isDone ? " " : <i className="fas fa-check"></i>}</button>
            <div id="task">{props.task.title}</div>
            <button className='button-delete' onClick={() => props.handleClick(props.task.id)} ><i className="fas fa-times"></i></button>
        </li>
    )
}

ReactDOM.render(<TodoList />, document.getElementById('root'))